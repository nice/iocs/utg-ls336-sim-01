#!/usr/bin/env iocsh.bash

################################################################
### requires
require(lakeshore336)

### lakeshore336 (ls1)
epicsEnvSet("IPADDR",               "127.0.0.1")
epicsEnvSet("IPPORT",               "7771")
epicsEnvSet("LOCATION",             "SIM: $(IPADDR):$(IPPORT)")
epicsEnvSet("SYS",                  "SIM:")
epicsEnvSet("DEV",                  "LS336-01")
epicsEnvSet("PREFIX",               "$(SYS)$(DEV)")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(lakeshore336_DIR)db/")

### E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

### load all db's
iocshLoad("$(lakeshore336_DIR)lakeshore336.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")

### install SNL curves
seq install_curve, "P=$(PREFIX), CurvePrefix=File"

iocInit()
